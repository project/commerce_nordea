��    '      T  5   �      `     a     |     �     �  \   �                5     N     V     ^  +   c     �  	   �  4   �  3   �               #     2  7   H  ^   �  #   �  )     %   -  1   S  1   �     �     �     �  
                  *  ]   /  (   �     �     �  �  �     p	     �	     �	     �	  g   �	      6
      W
  !   x
     �
     �
     �
  J   �
     �
     �
  7     7   :     r     �     �     �  @   �  _   �  (   W     �  3   �  T   �  .     %   G  %   m     �  
   �     �     �     �  R   �        '   9     a              
                 "          '                                                      	         %                            $                        !   &   #               Allow to save Credit Cards Basket Item Sending Check payment node availability Danish Define CRYPT_RSA_DISABLE_BLINDING as true in case of custom PHP build or PHP7 (experimental) Delayed url Disable rsa blinding Do not send basket items English Finnish Live Make a check that payment node is available No Norwegian Optional. Second redundant URL to the payment system Optional. Third redundant URL to the payment system Pay page URL 1 Pay page URL 2 Pay page URL 3 Payment page language Required. Path and filename of Nordea public key file Required. Path and filename of shop secret key file generated with Nordea key pair generator Required. URL to the payment system Required. Nordea Connect agreement code Return directly to shop after payment Select for which type of order should send items. Select language which will be use on payment page Send for all payment methods Shop private key filename Skip confirmation page Style code Swedish Swedish (Finland) Test Use of custom payment page template needs first to be uploaded and to be approved by Nordea Nordea Connect merchant agreement code Nordea public key filename Yes Project-Id-Version: Nordea DrupalCommerce
POT-Creation-Date: 2018-07-12 15:45+0300
PO-Revision-Date: 2018-07-12 15:45+0300
Last-Translator: 
Language-Team: 
Language: fi_FI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: t
X-Poedit-SearchPath-0: .
 Salli luottokorttien tallennus Ostoskoririvien lähtys Tarkista maksunodejen saatavuus Tanska Määrittele CRYPT_RSA_DISABLE_BLINDING arvoon true, jos käytät custom PHP-buildia tai PHP7-versiota. Viivästyneen maksun paluuosoite Poista RSA-blinding käytöstä. Älä lähetä ostoskorin rivejä Englanti Suomi Live Testi, joka suoritetaan nodeille ennen varsinaisen maksun lähettämistä. Ei Norja Valinnainen tieto. Maksujärjestelmän varaosoite(URL). Valinnainen tieto. Maksujärjestelmän varaosoite(URL). Maksusivun URL 1 Maksusivun URL 2 Maksusivun URL 3 Maksusivun kieli Vaadittu tieto. Polku ja tiedostonimi, jossa tiedosto sijaitsee. Vaadittu tieto. Polku ja tiedostonimi, josta löytyy generaattorilla luotu avainparin tiedosto. Vaadittu tieto. Maksujärjestelmän URL. Vaadittu tieto Palaa suoraan kauppaan onnistuneen maksun jälkeen. Valitse minkätyyppiset ostoskoririvit lähetetään maksujärjestelmälle tiedoksi. Valitse kieli, jota käytetään maksusivulla. Lähetä kaikkien maksutapojen osalta Kaupan privaatin avaimen tiedostonimi Ohita vahvistussivu Tyylikoodi Ruotsi Ruotsi(Suomi) Testi Käyttääksesi muokattua ulkoasua, ole yhteydessä Nordeaen sen aktivoimiseksi. Nordean kauppiastunnus Nordean julkisen avaimen tiedostonimi Kyllä 